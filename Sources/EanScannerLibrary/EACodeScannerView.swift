//
//  EACodeScannerMainView.swift
//  TestBarcode
//
//  Created by Matus Borodac on 19/02/2021.
//
import SwiftUI

public struct ScannerView: View {
    
    @State var torchIsTouched: Bool = false
    @Binding var codeIsFinded: Bool
    @Binding var code: String
    @Binding var showScanner: Bool
    var scannerViewController = ScannerViewController()
    
    public init(codeIsFinded: Binding<Bool>, code: Binding<String>, showScanner: Binding<Bool>) {
        self._codeIsFinded = codeIsFinded
        self._code = code
        self._showScanner = showScanner
    }
    
    public var body: some View {
        ZStack {
            QRCodeScan(scannerViewController: scannerViewController)
            VStack {
                Button(action: {
                    showScanner = false
                }) {
                   Text("Hotovo")
                    .frame(width: 65, height: 30)
                    .background(Color.white)
                    .cornerRadius(10)
                }
                .padding(.top)
                Spacer()
                Text("|")
                    .hidden()
                    .frame(width: 275, height: 275)
                    .overlay(
                        Rectangle()
                            .stroke(Color.blue, style: StrokeStyle(lineWidth: 5.0,lineCap: .round, lineJoin: .bevel, dash: [60, 215], dashPhase: 29)))
                .padding(.vertical, 20) 
                Spacer()
                HStack {
                    Button(action: {
                        torchIsTouched.toggle()
                        scannerViewController.toggleTorch(on: torchIsTouched)
                    }, label: {
                        Image(systemName: torchIsTouched ? "bolt.fill" : "bolt.slash.fill")
                            .imageScale(.large) 
                            .foregroundColor(torchIsTouched ? Color.yellow : Color.blue)
                            .padding()
                    })
                }
                .background(Color.white)
                .cornerRadius(10)
            }.padding()
        }
        .onAppear {
            scannerViewController.callingView = self
        }
        .edgesIgnoringSafeArea(.all)
    }
}


extension Binding {
    func onChange(_ handler: @escaping (Value) -> Void) -> Binding<Value> {
        Binding(
            get: { self.wrappedValue },
            set: { newValue in
                self.wrappedValue = newValue
                handler(newValue)
            }
        )
    }
}
