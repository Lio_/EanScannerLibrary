    import XCTest
    import SwiftUI
    @testable import EanScannerLibrary

    final class EanScannerLibraryTests: XCTestCase {
        @State var codeIsFinded: Bool
        @State var code: String
        @State var showScanner: Bool
        @State var showText: Bool = false
        
        public init(codeIsFinded: Bool, code: String, showScanner: Bool) {
            self.code = code
            self.codeIsFinded = codeIsFinded
            self.showScanner = showScanner
            super.init()
        }
        
        var body: some View {
            
            NavigationView {
                VStack {
                    if showText {
                        Text("HURAAAAAAAAAAAAA").bold()
                    }
                    Button(action: {
                        self.showScanner = true
                    }) {
                        Image(systemName: "barcode.viewfinder")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 25.0, height: 25.0)
                            .padding()
                            .padding(.bottom, -30)
                    }.fullScreenCover(isPresented: $showScanner, onDismiss: {
                        if self.codeIsFinded {
                            self.showText = true
                        }
                        self.code = ""
                        self.codeIsFinded = false
                    }, content: {
                        EanScannerLibrary.ScannerView(codeIsFinded: self.$codeIsFinded, code: self.$code, showScanner: self.$showScanner)
                    })
                }
            }
        }
    }
    
